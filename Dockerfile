#FROM golang:latest
#
#ENV export GO111MODULE=on
#
#RUN go get "github.com/prometheus/client_golang/prometheus/promhttp"
#RUN go get "github.com/slok/go-http-metrics/metrics/prometheus"
#RUN go get "github.com/slok/go-http-metrics/middleware"
#RUN go get "github.com/slok/go-http-metrics/middleware/std"
#
#COPY main.go main.go
#
#RUN go run -mod=vendor main.go
#
#EXPOSE 8080 8081

FROM golang:1.14
WORKDIR /go/src/app
COPY main.go main.go

RUN go get -d -v "github.com/prometheus/client_golang/prometheus/promhttp"
RUN go get -d -v "github.com/slok/go-http-metrics/metrics/prometheus"
RUN go get -d -v "github.com/slok/go-http-metrics/middleware"
RUN go get -d -v "github.com/slok/go-http-metrics/middleware/std"

RUN go install -v "github.com/prometheus/client_golang/prometheus/promhttp"
RUN go install -v "github.com/slok/go-http-metrics/metrics/prometheus"
RUN go install -v "github.com/slok/go-http-metrics/middleware"
RUN go install -v "github.com/slok/go-http-metrics/middleware/std"

EXPOSE 8080 8081

CMD ["go", "run", "main.go"]